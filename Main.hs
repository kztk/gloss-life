import Graphics.Gloss
import Data.Array.Repa (Array, U, Z(..), (:.)(..), (!), DIM2)
import qualified Data.Array.Repa as R 
import Data.Array.Repa.Algorithms.Randomish

import Data.List ((\\))
-- import Debug.Trace

import System.Random
import System.IO.Unsafe

type World = Array U DIM2 Int 

maxI :: Num a => a
maxI = 200
maxJ :: Num a => a 
maxJ = 200

cellSize :: Float
cellSize = 5

render :: World -> Picture 
render arr = pictures $ do
  i <- [0..maxI-1]
  j <- [0..maxJ-1]
  let c = if arr ! (Z :. i :. j) > 0 then
            white
          else
            makeColor 0.5 0.5 0.5 1
  return $ color c $
    translate (cellSize*(fromIntegral j - maxJ/2))
              (cellSize*(maxI/2 - fromIntegral i)) $
    rectangleSolid (cellSize-1) (cellSize-1)

neighbors :: [(Int, Int)]
neighbors = [ (i,j) | i <- [-1,0,1], j <- [-1,0,1] ] \\ [(0,0)]
            
nextWorld :: World -> World 
nextWorld arr =
  unsafePerformIO $ R.computeP $ R.traverse arr id $
   (\f (ix@(Z :. i :. j)) ->
     let nS = sum [safeIx f (i + di) (j + dj) | (di,dj) <- neighbors ]
     in if f ix > 0 then 
          if nS < 2 || nS > 3 then
            0
          else
            1
        else
          if nS == 3 then
            1
          else
            0)
  where
    safeIx f i j =
      if i < 0 || i >= maxI || j < 0 || j >= maxJ then
        0
      else
        f (Z :. i :. j)
    
            


main = do
  seed <- randomIO :: IO Int
  let arr = randomishIntArray (Z :. maxI :. maxJ) 0 1 seed   
  simulate (InWindow "Conway's Life Game" (800, 600) (80, 80))
           black 10
           arr
           render (\_ _ -> nextWorld) 
